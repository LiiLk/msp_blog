// import mongoose from "mongoose";

const mongoose = require("mongoose");

const UtilisateurSchema = new mongoose.Schema(
    {
        Nom_user: {
            type: String,
            required: true,
            unique: true,
        },
        Email_User: {
            type: String,
            required: true,
            unique: true,
        }, Mot_de_passe: {
            type: String,
            required: true,
        }, Photo: {
            type: String,
            default: "",
        }, role: {
            type: String,
            required: true,
        }
           
    },
    { timestamps: true }
);
module.exports = mongoose.model("Utilisateur", UtilisateurSchema);   