// import express from 'express';
// import dotenv from 'dotenv';
// import cors from 'cors';
// import mongoose from "mongoose";
// import authRoute from "./routes/auth.js";
// import userRoute from "./routes/users.js";
// import postRoute from "./routes/posts.js";
// import categoryRoute from "./routes/categories.js";
// import multer from "multer";
// import path from "path";

const express = require("express");
const dotenv = require("dotenv");
const app = express();
const mongoose = require("mongoose");
const authRoute = require("./routes/auth");
const userRoute = require("./routes/users");
const articleRoute = require("./routes/articles");
const categoryRoute = require("./routes/categories");
const roleRoute = require("./routes/role");
const multer = require("multer");
const path = require("path");
const cors = require("cors");
const bodyParser = require("body-parser");

dotenv.config();
app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use("/images", express.static(path.join(__dirname, "/images")));
mongoose
  .connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(console.log("Connected to MongoDB"))
  .catch((err) => console.log(err));

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "images");
  },
  filename: (req, file, cb) => {
    cb(null, req.body.name);
  },
});

const upload = multer({ storage: storage });
app.post("/api/upload", upload.single("file"), (req, res) => {
  res.status(200).json("Le fichier a bien été uploader.");
});

app.use("/auth", authRoute);
app.use("/utilisateur", userRoute);
app.use("/articles", articleRoute);
app.use("/categories", categoryRoute);
app.use("/role", roleRoute);
app.listen("5000", () => {
  console.log("Backend is running.");
});
