const router = require('express').Router();
const User = require('../models/User');
const Article = require('../models/Article');

//CREATE Article
router.post("/", async (req, res) => {
    const newArticle = new Article(req.body);
    console.log(req);
    try {
        const savedArticle = await newArticle.save();
        res.status(200).json(savedArticle);
    } catch (err) {
        res.status(500).json(err)
    }   

});


//UPDATE Article
router.put("/:id", async (req, res) => {
    try {
        const article = await Article.findById(req.params.id);

        if (article.username === req.body.username) {
            try {
                const updatedArticle = await Article.findByIdAndUpdate(req.params.id, {
                    $set: req.body
                },
                    { new: true }
                );
                res.status(200).json(updatedArticle);
            } catch (err) {
                res.status(500).json(err);
            }
        } else {
            res.status(401).json("Tu peux mettre à jour que tes articles");
        }


    }catch (err) {
    res.status(500).json(err);
    }
});

//DELETE Article
router.delete("/:id", async (req, res) => {
    try {
        const article = await Article.findById(req.params.id);

        if (article.username === req.body.username) {
            try {
              await article.delete();
              res.status(200).json("L'article a bien été supprimer");
            } catch (err) {
                res.status(500).json(err);
            }
        } else {
            res.status(401).json("Tu peux supprimer que tes article");
        }


    }catch (err) {
    res.status(500).json(err);
    }
});



//GET ARTICLE
router.get("/:id", async (req, res) => {
    try {
        const article = await Article.findById(req.params.id);
        res.status(200).json(article);
    } catch (err) {
        res.status(500).json(err);
    }
});


//GET ALL ARTICLES
router.get("/", async (req, res) => {
    const username = req.query.user;
    const catName = req.query.cat;
    try {
        let articles;
        if(username){
            articles = await Article.find({username})
        } else if(catName){
            //va chercher la catégorie envoie en requete par l'utilisateur
            articles = await Article.find({categories:{
                //si dans($in) categories il voit la catégorie il affiche
                $in:[catName]
            }})
        }else {
            articles = await Article.find();
        }
        res.status(200).json(articles);
    } catch (err) {
        res.status(500).json(err);
    }
});


module.exports = router;