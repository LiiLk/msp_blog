const Utilisateur = require("../models/User");
const bcrypt = require("bcrypt");
const router=require("express").Router();

//INSCRIPTION
//create something : post
//update something exist : put
//delete something : delete
//get something : get

// req = request sending in server when here you create an user
// res = response from server after the request "create an user"
router.post("/inscription", async (req, res) => {
  //try connect database and create user
  
  try {
    //bcrypto make the password crypted with the hash and the genSalt
    console.log("test avant pwd");
    
    const salt = await bcrypt.genSalt(10);
    console.log(req.body);
    console.log("Mot de passe pre hashed", req.body.Mot_de_passe);
    const hashedPass = await bcrypt.hash(req.body.Mot_de_passe, salt);
    console.log("hashedpass effectuer");
    
    // req.body = the content of this request like user password etc..
    const newUser = new Utilisateur({
      Nom_user: req.body.Nom_user,
      Email_User: req.body.Email_User,
      Mot_de_passe: hashedPass,
      role: req.body.role,
    });

    console.log(newUser);

    const user = await newUser.save();
    res.status(200).json(user);
  } catch (err) {
    //else if we have error we can do this catch
    res.status(500).send("Problème au niveau de l'envoie de la requete"); //Something wrong
    console.log(err) 
  }
});

//CONNEXION
router.post("/login", async (req, res) => {
  console.log("test", req.body);
  try {
    //regarde si le Nom_user existe dans le login
    const user = await Utilisateur.findOne({ Nom_user: req.body.Nom_user });
    if (!user) {
      res.status(404).json("Mauvais utilisateur");
      return;
    }
    console.log("test1");

    const valider = await bcrypt.compare(req.body.Mot_de_passe, user.Mot_de_passe);
    console.log("test2");
    if (!valider) {
      res.status(403).json("Mauvais mot de passe");
      return;
    }

    const { Mot_de_passe, ...others } = user._doc;
    res.status(200).json(others);
  } catch (err) {
    console.error(err);
    res.status(500).json(err);
  }
});

module.exports = router;
