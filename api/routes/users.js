const router = require("express").Router();
const User = require("../models/User");
const Article = require("../models/Article");
const bcrypt = require("bcrypt");
const Role = require("../models/Role");

router.put("/:id", async (req, res) => {
  //Compare the request id user with the real id account if the not same return error server
  if (req.body.id_User !== req.params.id) {
    res.status(401).json("Tu peux mettre à jour que ton compte.");
    return;
  }

  if (req.body.Mot_de_passe) {
    const salt = await bcrypt.genSalt(10);
    req.body.Mot_de_passe = await bcrypt.hash(req.body.Mot_de_passe, salt);
  }
  try {
    const updatedUser = await User.findByIdAndUpdate(
      req.params.id,
      {
        $set: req.body,
      },
      { new: true }
    );
    res.status(200).json(updatedUser);
  } catch (err) {
    //else if we have error we can do this catch
    res.status(500).json(err); //Something wrong
  }
  //try connect database and create user
});


//Delete
router.delete("/:id", async (req, res) => {
  //Compare the request id user with the real id account if the not same return error server
  if (req.body.id_User === req.params.id) {
    try {
      const user = await User.findById(req.params.id);
      try {
        await Article.deleteMany({ username: user.username });
        await User.findByIdAndDelete(req.params.id);
        res.status(200).json("L'utilisateur a ete supprimer...");
      } catch (err) {
        //else if we have error we can do this catch
        res.status(500).json(err); //Something wrong
      }
    } catch (err) {
      res.status(404).json("Utilisateur non trouvé");
    }
  } else {
    res.status(401).json("Tu peux supprimer que ton compte.");
  }
  //try connect database and create user
});

//GET USER
router.get("/:id", async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    const { Mot_de_passe, ...others } = user._doc;
    res.status(200).json(others);
  } catch (err) {
    res.status(500).json(err);
  }
});
//GET ALL USER
router.get("/", async (req, res)=>{
  
  try {
      const userGet = await User.find();
      
      res.status(200).json(userGet); 
  }catch(err){
      res.status(500).json(err);
  }
});

module.exports = router;
