const router = require("express").Router();
const Role = require("../models/Role");

//CREATE ROLE
router.post("/", async (req, res) => {
    const newRole = new Role(req.body);
    try {
        const savedRole = await newRole.save();
        res.status(201).json(savedRole);
    } catch (err) {
        res.status(500).json(err)
    }

});

//GET ALL ROLE
router.get("/", async (req, res)=>{
    try {
        const roleGet = await Role.find();
        res.status(200).json(roleGet); 
    }catch(err){
        res.status(500).json(err);
    }
});

module.exports = router;